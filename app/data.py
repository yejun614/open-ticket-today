import hashlib
SHA256 = lambda x: hashlib.sha256(x.encode()).hexdigest()
SHA512 = lambda x: hashlib.sha512(x.encode()).hexdigest()

import json
from time import time as Time, \
                 strftime, \
                 gmtime

from redis import Redis
database = Redis(host='db', port=6379, db=0)

def setRedis(**kwargs):
    database = Redis(**kwargs)
    return database.ping()

class Ticket:
    headers = [
        'auth',
        'seat',
        'form',
        'price',
        'username',
        'datetime'
    ]

    def all(self, start, end):
        data = []
        keys = [i for i in database.scan_iter('ticket:*')]
        length = len(keys)

        # Slice start and end points
        if start < 0:
            start = 0
        
        if end > length:
            end = length

        keys = keys[start:end]

        for key in keys:
            db = json.loads( database.get(key) )
            db['key'] = key.decode()

            data.append(db)

        return data

    def getkey(self, username=None, password=None):
        if username: username = SHA512(username)
        if password: password = SHA512(password)

        keys = [i for i in database.scan_iter('ticket:%s:%s' % (username, password))]

        if len(keys) > 0:
            return keys[0]
        else:
            return None

    def login(self, username, password):
        keys = [i for i in database.scan_iter('ticket:%s:%s' % (username, password))]

        if len(keys) > 0:
            key = keys[0]
            return self.get(key)
        else:
            return False

    def data_check(self, data, size=True):
        # Check data keys
        header_check = [True if name in self.headers else False for name in data]

        # Invalid data
        if (False in header_check):
            return False

        if (size) and (len(self.headers) != len(data)):
            return False

        # Correct data
        return True

    def create(self, username, password, data, encrypt=True):
        # Check header
        if not self.data_check(data): return False

        # Generate Key
        if encrypt:
            username = SHA512(username)
            password = SHA512(password)
        
        key = 'ticket:%s:%s' % (username, password)

        # Check username exist
        keys = [i for i in database.scan_iter('ticket:%s:*' % (username))]
        if len(keys) > 0:
            return False

        # Data to JSON String
        value = json.dumps(data)

        # Query to Database
        database.mset({key:value})

        # Return key
        return key

    def delete(self, key):
        return database.delete(key)

    def get(self, key):
        return json.loads( database.get(key) )

    def update(self, key, data):
        # Check key exist
        if database.exists(key) <= 0: return False

        # Check data headers
        if not self.data_check(data, size=False): return False

        # Get data
        origin = database.get( key )
        origin = json.loads( origin.decode() )

        # Update data
        for k in data:
            origin[k] = data[k]

        # Data to JSON String
        value = json.dumps(origin)

        return database.mset({key:value})

    def gen_code(self, key):
        # Check key exist
        if database.exists(key) <= 0: return False

        # Generate CODE
        code = SHA512( str(Time()) )

        # Set database
        database.mset({ 'code:%s' % code: key })
        database.expire('code:%s' % code, 300)
        return code

    def check_code(self, code):
        key = 'code:' + code

        # Check code exist
        if database.exists(key) <= 0:
            return False

        # Get data
        ticket = database.get(key)
        data = self.get(ticket)

        # Return data
        return data

    def change_seat_password(self, password, new_password):
        keys = [i for i in database.scan_iter('realtime:seat:*')]
        for key in keys:
            if (database.get(key) == password):
                database.mset({key:new_password})

    def change_username(self, key, username):
        # Check key exist
        if database.exists(key) <= 0: return False

        # Set key
        keys = key.split(':')
        keys[1] = username

        newkey = ''.join(map(lambda x: x+':', keys))[:-1]

        # Change seat password
        self.change_seat_password(key, newkey)

        # Change password
        database.rename(key, newkey)
        return newkey

    def change_password(self, key, password):
        # Check key exist
        if database.exists(key) <= 0: return False

        # Set key
        keys = key.split(':')
        keys[2] = password

        newkey = ''.join(map(lambda x: x+':', keys))[:-1]

        # Change seat password
        self.change_seat_password(key, newkey)

        # Change password
        database.rename(key, newkey)
        return newkey

class Config:
    def load(self, filename='settings.json'):
        data = None
        with open(filename, 'r') as file:
            data = json.loads(file.read())

        for name in data:
            key = 'config:' + name
            value = data[name]

            database.mset({key:value})

    def set(self, name, value):
        key = 'config:' + name
        return database.mset({key:value})

    def get(self, name):
        return database.get('config:' + name).decode()

class RealtimeSeat:
    def __init__(self, length):
        self.length = int(length)

    def all(self):
        keys = [i for i in database.scan_iter('realtime:seat:*')]
        num = []

        for key in keys:
            num.append( key.decode().split(':')[-1] )

        return num

    def put(self, num, password):
        num = int(num)

        # Check num
        if num not in range(0, self.length):
            return False

        # Generate key
        key = 'realtime:seat:%d' % (num)

        # Check already seat is selected
        if database.get(key) != None:
            return False

        # Select seat and start TTL (2 Minutes)
        database.mset({key:password})
        database.expire(key, 120)

        return True

    def check_password(self, num, password):
        num = int(num)

        # Generate key
        key = 'realtime:seat:%d' % (num)

        # Check password
        db_password = database.get(key).decode()
        if (db_password == None) or (db_password != password):
            return False
        else:
            return key

    def pop(self, num, password):
        num = int(num)

        # Check password
        key = self.check_password(num, password)
        if not key: return False

        # Cancel seat
        return database.delete(key)

    def check(self, num, password):
        num = int(num)

        # Check password
        key = self.check_password(num, password)
        if not key: return False

        # Update TTL
        database.mset({key:password})
        database.expire(key, 120)

        return True

    def confirm(self, num, password, ticket):
        num = int(num)

        # Check password
        key = self.check_password(num, password)
        if not key: return False

        # Confirm the seat
        return database.mset({key:ticket})

# Simple running the methods
ticket = Ticket()
config = Config()
seat = RealtimeSeat( config.get('seat:length') )
