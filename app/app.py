from flask import Flask, \
                  render_template, \
                  jsonify, \
                  redirect, \
                  url_for

from route import *

import os
import json
import redis

database = redis.Redis(host='db', port=6379, db=0)
def load():
    import settings
    data = settings.config

    for name in data:
        key = 'config:' + name
        value = data[name]
        if type(value) in [list, dict]:
            value = json.dumps(value)

        database.mset({key:value})

load()
import data

app = Flask(__name__)
app.secret_key = os.urandom(512)

app.register_blueprint(user.api, url_prefix='/user')
app.register_blueprint(manager.api, url_prefix='/manager')
app.register_blueprint(seat.api, url_prefix='/seat')

@app.route('/')
def index_page():
    return redirect(url_for('user.purchase'))

@app.route('/api/form/data', methods=['GET', 'POST'])
def form_data():
    form = data.config.get('form')

    return jsonify(success=True, data=json.loads(form))
