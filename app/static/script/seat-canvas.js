
class SeatCanvasInstance {
    constructor (element) {
        this.element = element;
        this.seat = [];
        this.width = 0;
        this.height = 0;
        this.background = {};
        this.option = {};

        this.checkedSeats = {};
        this.checkedSeatUpdate = {};

        this.currentSeatNum = undefined;
        this.currentOption = undefined;

        // Get seat
        $.ajax({
            url: '/seat/data',
            mehtod: 'GET',
            dataType: 'json'
        })
        .done(response => {
            this.seat = response.data;
            this.width = response.width;
            this.height = response.height;
            this.background = response.background;
            this.option = response.option;

            // Make element
            this.make_element();

            // Update
            this.startUpdate();
        });
        
    }

    make_element () {
        const element = this.element;

        // Add class
        $(element).addClass('seat-canvas');

        // Set width and height
        $(element).css('width', this.width);
        $(element).css('height', this.height);

        // Set background
        Object.keys(this.background).forEach(key => {
            $(element).css('background-' + key, this.background[key]);
        })

        // Make modal element
        let optionHTML = ``;

        Object.keys(this.option).forEach(option => {
            optionHTML += `<a class="list-group-item list-group-item-action" value="${option}">`;
            optionHTML += `${option} (${this.option[option]})`;
            optionHTML += `</a>`;
        });

        const modalHTML = `
        <div
            class="modal fade"
            id="seat-canvas-option-selector"
            data-backdrop="static"
            tabindex="-1",
            role="dialog"
            aria-hidden="true">

            <div class="modal-dialog modia-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Select Seat Option</h5>
                        <button type="button" class="close" data-dismiss="modal" aira-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="list-group">
                            ${optionHTML}
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="seat-canvas-modal-btn">Select</button>
                    </div>
                </div>
            </div>
        </div>
        `;
        $("body").append(modalHTML);

        // Add seats
        this.seat.forEach(seat => {
            let html = `<div class="seat seat_${seat.num}" value="${seat.num}">${seat.num}</div>`;
            $(element).append(html);

            // Set position
            const pos = seat.position.split(', ');
            const width = pos[0].slice(1)
            const height = pos[1].slice(0, pos[1].length - 1);

            $(element).find(`.seat_${seat.num}`).css('transform', `translate(${width}px, ${height}px)`);

            // Add event
            $(element).find(`.seat_${seat.num}`).click((event) => {
                const target = event.currentTarget;
                const num = $(target).attr('value');

                if ($(target).hasClass('selected')) {
                    this.put_back(num);

                } else if ($(target).hasClass('active')) {
                    alert('This seat is already reserved');

                } else {
                    this.currentSeatNum = num;
                    $('#seat-canvas-option-selector').modal('show');
                }
            });
        });

        // Add Modal Button Event
        $('#seat-canvas-option-selector .list-group-item').click((event) => {
            const target = event.currentTarget;
            this.currentOption = $(target).attr('value');

            $('#seat-canvas-option-selector .list-group-item.active').removeClass('active');
            $(target).addClass('active');
        });

        $('#seat-canvas-modal-btn').click(() => {
            this.put(this.currentSeatNum);

            $('#seat-canvas-option-selector .list-group-item.active').removeClass('active');
            $('#seat-canvas-option-selector').modal('hide');
        });
    }

    check (num) {
        const password = this.checkedSeats[num].password;

        $.ajax({
            url: '/seat/check',
            method: 'POST',
            data: JSON.stringify({
                'num': num,
                'password': password
            }),
            contentType: 'application/json',
            dataType: 'json'
        })
        .done(response => {
            if (!response.success) {
                console.log(`%c${response}`, 'color: red;');
                alert('Can not update the seat')
            }
        })
        .fail((jqXHR, textStatus) => {
            console.log(`%c${jqXHR}`, 'color: red;');
            alert('System Error, please contact with manager');
        })
    }

    put (num) {
        // Send request
        $.ajax({
            url: '/seat/put',
            method: 'POST',
            data: JSON.stringify({'num': num}),
            contentType: 'application/json',
            dataType: 'json'
        })
        .done(response => {
            if (response.success) {
                // Save seat password & option
                this.checkedSeats[num] = {
                    'password': response.password,
                    'option': this.currentOption,
                    'position': this.seat[num].position
                }

                // Add class
                $(this.element).find('.seat_' + num).addClass('selected');

                // Add check event
                this.checkedSeatUpdate[num] = setInterval(() => { this.check(num); }, 1000 * 60);

            } else {
                alert('This seat is already reserved');
            }
        })
        .fail((jqXHR, statusText) => {
            console.log(`%c${jqXHR}`, 'color: red;');
            alert('System Error, please contact with manager');
        });
    }

    put_back (num) {
        let password = this.checkedSeats[num].password;

        $.ajax({
            url: '/seat/back',
            method: 'POST',
            data: JSON.stringify({
                'num': num,
                'password': password
            }),
            contentType: 'application/json',
            dataType: 'json'
        })
        .done(response => {
            if (!response.success) {
                console.log(`%c${response}`, 'color: red;');
                alert('System Error, please contact with manager');
            } else {
                // Remove class
                $(this.element).find('.seat_' + num).removeClass('selected');

                // Remove interval and data
                clearInterval(this.checkedSeatUpdate[num]);
                delete this.checkedSeatUpdate[num];
            }
        })
        .fail((jqXHR, statusText) => {
            console.log(`%c${jqXHR}`, 'color: red;')
            alert('System Error, please contact with manager');
        });
    }

    cancel () {
        Object.keys(this.checkedSeats).forEach(seat => {
            this.put_back(seat)
        });
    }

    confirm (ticket) {
        const seats = this.checkedSeats;

        $.ajax({
            url: '/seat/confirm',
            method: 'POST',
            data: JSON.stringify({
                'seats': seats,
                'ticket': ticket
            }),
            contentType: 'application/json',
            dataType: 'json'
        })
        .done(response => {
            if (!response.success) {
                console.log(`%c${response}`, 'color: red;');
                alert('System Error, please contact with manager');
            } else {
                this.checkedSeatUpdate.forEach(seat => { clearInterval(seat) });
            }
        })
    }

    update () {
        const element = this.element;

        $.ajax({
            url: '/seat/get',
            method: 'GET',
            dataType: 'json'
        })
        .done(response => {
            $(element).find('.seat').removeClass('active');

            response.data.forEach(seat => {
                $(element).find('.seat_' + seat).addClass('active');
            });
        });
    }

    startUpdate () {
        this.updateFunc = setInterval(function() {this.update()}.bind(this), 500);
    }

    stopUpdate() {
        clearInterval(this.updateFunc);
    }

    getTotalPrice () {
        let currency = '';
        let total = 0;

        Object.keys(this.checkedSeats).forEach(seat => {
            const option_name = this.checkedSeats[seat].option
            const option = this.option[option_name].split(' ')

            currency = option[0];
            total += Number(option[1]);
        });

        return `${currency} ${total}`;
    }

    getSeatTable () {
        let result = []
        Object.keys(this.checkedSeats).forEach(seat => {
            result.push({
                'num': seat,
                'position': this.checkedSeats[seat].position,
                'option': this.checkedSeats[seat].option
            });
        });

        return result;
    }
}
