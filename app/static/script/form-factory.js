var form = [];
$.ajax({
    url: '/api/form/data',
    method: 'GET',
    dataType: 'json'
}).done(response => {form = response.data});

function make_form(element, form_data) {
    // Clear form
    $(element)[0].innerHTML = '';

    form.forEach(data => {
        // 1. Make basic form
        let input = ``;
        
        if (data.type == "text") {
            input = `<input type="text" class="form-control" value="${data.value}" placeholder="text">`;
        } else if (data.type == "password") {
            input = `<input type="password" class="form-control" value="${data.value}" placeholder="password">`;
        } else if (data.type == "checkbox") {
            input = `
            <div class="form-check">
                <input class="form-check-input" type="checkbox">
                <label class="form-check-label">${data.value}</label>
            </div>
            `;
        } else if (data.type == "radio") {
            input = `
            <fieldset class="form-group">
            <div class="row">
                <legend class="col-form-label col-sm-2 pt-0">${data.name}</legend>
                <div class="col-sm-10">`;

            const option = data.option;
            for (let n=0; n<option.length; n++) {
                let checked = '';
                if (data.checked == n){
                    checked = 'checked';
                }

                input += `
                <div class="form-check">
                    <input name="${data.name}" class="form-check-input" type="radio" value="${option[n]}" ${checked}>
                    <label class="form-check-label">${option[n]}</label>
                </div>`;
            }

            input += `
                </div>
            </div>
            </fieldset>`;

        } else {
            console.log("%cMake Form Error: unknown data type", "color:red");
        }

        // 2. Add label (exception in radio)
        let html = '';
        if (data.type == "radio") {
            html = input;
        } else {
            html = `
            <div class="form-group row">
                <label class="uk-text-secondary">${data.name}</label>
                <div class="uk-width-1-1">${input}</div>
            </div>`;
        }

        // 3. Append element
        $(element).append(html);

        // Form data
        if (form_data) {
            set_form(element, form_data);
        }
    });
}

function set_form(element, dataset) {
    const el = $($(element)[0]).children();

    for (let n=0; n<el.length; n++) {
        const data = dataset[ form[n].name ];
        const type = form[n].type;
        const children = $(el[n]).children();

        if (type == 'text' || type == 'password') {
            $(children).find('input').val(data);
        } else if (type == 'checkbox') {
            $(children).find('input')[0].checked = data;
        } else if (type == 'radio') {
            $(children).find('input:checked').removeAttr('checked');
            $(children).find(`input[value="${data}"]`).attr('checked', '');
        } else {
            console.log("%cMake Form Error: unknown data type", "color:red");
        }
    }

    $(element).find('input').attr('disabled', '');
    $(element).find('input').addClass('detail');
}

function read_form(element) {
    let result = {};
    const el = $($(element)[0]).children();

    for (let n=0; n<el.length; n++) {
        const children = $(el[n]).children();

        const type = form[n].type;
        const name = form[n].name;

        if (type == 'text' || type == 'password') {
            result[name] = $(children).find('input').val();
        } else if (type == 'checkbox') {
            result[name] = $(children).find('input')[0].checked;
        } else if (type == 'radio') {
            result[name] = $(children).find('input:checked').val();
        } else {
            console.log("%cMake Form Error: unknown data type", "color:red");
        }

        // Check blink element
        if (result[name] == "") {
            alert('Please input all of the form');
            return;
        }
    }

    return result
}
