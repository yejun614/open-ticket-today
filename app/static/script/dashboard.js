function reload_list() {
    // Get current page number
    currentPage = pageNumber;

    // Clear list
    $('#table tbody')[0].innerHTML = '';

    // Reload list
    pageNumber = 0;
    for (let n=0; n<currentPage; n++) {
        loadNextTickets();
    }
}

function update_ticket() {
    // Get key, data
    let element = '';
    if ($('#card').css('display') == 'none') {
        element = '#modal';
    } else {
        element = '#card';
    }

    const key = $(element).find('input[name="ticket_key"]').val();

    let data = {
        username: $(element).find('.detail.username').val(),
        form: read_form(element + ' .detail.form'),
        auth: $(element).find('.detail.auth')[0].innerHTML
    };

    // Request
    $.ajax({
        url: '/manager/dashboard/ticket/update',
        method: 'POST',
        data: JSON.stringify({
            'key': key,
            'data': data
        }),
        contentType: 'application/json',
        dataType: 'json'
    })
    .done(response => {
        if (response.success) {
            // Relaod ticket detail and list
            ticket_detail(response.key);
            reload_list();

            // Notification
            UIkit.notification({
                message: 'Ticket updated',
                pos: 'bottom-left',
                timeout: 5000
            });
        }
    })
    .fail((jqXHR, textStatus) => {
        console.log(jqXHR);
        UIkit.notification({
            message: 'Ticket Update Error',
            status: 'danger',
            pos: 'bottom-left',
            timeout: 5000
        });
    });
}

function delete_ticket() {
    // Get ticket key
    let key = '';
    if ($('#card').css('display') == 'none') {
        key = $('#modal').find('input[name="ticket_key"]').val();
    } else {
        key = $('#card').find('input[name="ticket_key"]').val();
    }

    // Request
    $.ajax({
        url: '/manager/dashboard/ticket/delete',
        method: 'POST',
        data: JSON.stringify({
            'key': key
        }),
        contentType: 'application/json',
        dataType: 'json'
    })
    .done(response => {
        if (response.success) {
            // Close viewer
            $('#card').css('visibility', 'hidden');
            UIkit.modal('#modal').hide();

            // List reload
            reload_list();

            // Notification
            UIkit.notification({
                message: 'Ticket deleted',
                pos: 'bottom-left',
                timeout: 5000
            });
        } else {
            UIkit.notification({
                message: 'Can not found ticket',
                status: 'danger',
                pos: 'bottom-left',
                timeout: 5000
            });
        }
    })
    .fail((jqXHR, textStatus) => {
        console.log(jqXHR);
        UIkit.notification({
            message: 'System Error! please contact with system manager',
            status: 'danger',
            pos: 'bottom-left',
            timeout: 5000
        });
    });

    // Modal hide
    $('#modal-delete').modal('hide');
}

function change_password() {
    const password = $('#modal-password input[name="password"]').val();
    const password_confirm = $('#modal-password input[name="password-confirm"]').val();

    $('#modal-password input[name="password"]').val('')
    $('#modal-password input[name="password-confirm"]').val('')

    if (password == password_confirm) {
        let key = '';
        if ($('#card').css('display') == 'none') {
            key = $('#modal').find('input[name="ticket_key"]').val();
        } else {
            key = $('#card').find('input[name="ticket_key"]').val();
        }

        // Request
        $.ajax({
            url: '/manager/dashboard/ticket/password',
            method: 'POST',
            data: JSON.stringify({
                'key': key,
                'password': sha512(password)
            }),
            contentType: 'application/json',
            dataType: 'json'
        })
        .done(response => {
            if (response.success) {
                // Reset
                ticket_detail(response.key);
                reload_list();

                // Notification
                UIkit.notification({
                    message: 'Change Ticket Password',
                    pos: 'bottom-left',
                    timeout: 5000
                });
            } else {
                UIkit.notification({
                    message: 'Can not found ticket',
                    status: 'danger',
                    pos: 'bottom-left',
                    timeout: 5000
                });
            }
        })
        .fail((jqXHR, textStatus) => {
            console.log(jqXHR);
            UIkit.notification({
                message: 'System Error! please contact with system manager',
                status: 'danger',
                pos: 'bottom-left',
                timeout: 5000
            });
        });

        // Modal hide
        $('#modal-password').modal('hide');
    } else {
        $('#modal-password .message')[0].innerHTML = 'Password and password confirm is different';

        setTimeout(() => {
            $('#modal-password .message')[0].innerHTML = '';
        }, 1000);
    }
}

var unlockElements = undefined;
function unlock_ticket() {
    if (unlockElements) {
        unlockElements.attr('disabled', '');
        unlockElements = undefined;
    } else {
        unlockElements = $('.detail[disabled]');
        unlockElements.removeAttr('disabled');

        // Do not edit total price
        $('.detail.price').attr('disabled', '');
    }
}

function read_ticket_detail(element, key, data) {
    // Ticket key
    $(element).find('input[name="ticket_key"]').val(key);

    // Username
    $(element).find('.detail.username').val(data.username);

    // Auth
    $(element).find('.detail.auth').removeClass('uk-label-danger');
    $(element).find('.detail.auth').removeClass('uk-label-warning');
    $(element).find('.detail.auth').removeClass('uk-label-primary');

    var auth = data.auth;
    $(element).find('.detail.auth')[0].innerHTML = auth;
    if (auth == 'auth') {
        $(element).find('.detail.auth').addClass('uk-label-primary');
    } else if (auth == 'unauth' || auth == 'cancel') {
        $(element).find('.detail.auth').addClass('uk-label-danger');
    } else {
        $(element).find('.detail.auth').addClass('uk-label-warning');
    }

    // Seat
    const seat_table = $(element).find('.detail.seat tbody')
    const seat = data.seat;

    seat_table[0].innerHTML = '';
    seat.forEach(element => {
        let html = `
        <tr>
            <td>${element.num}</td>
            <td>${element.position}</td>
            <td>${element.option}</td>
        </tr>`;

        seat_table.append(html);
    });

    // Form
    make_form(element + ' .detail.form', data.form);

    // Total Price
    $(element).find('.detail.price').val(data.price);
}

function ticket_detail(ticket_id) {
    $.ajax({
        url: '/manager/dashboard/ticket/get',
        method: 'POST',
        data: JSON.stringify({
            'key': ticket_id
        }),
        contentType: 'application/json',
        dataType: 'json'
    })
    .done((response) => {
        if (response.success) {
            if ($('#card').css('display') == 'none') {
                $('#card').css('visibility', 'hidden');

                let html = $('#detail')[0].innerHTML;
                $('#modal .detail')[0].innerHTML = html;

                read_ticket_detail('#modal .detail', ticket_id, response.data);
                UIkit.modal('#modal').show();

            } else {
                read_ticket_detail('#detail', ticket_id, response.data);
                $('#card').css('visibility', 'visible');
            }

        } else {
            UIkit.notification({
                message: response.message,
                status: 'danger',
                pos: 'bottom-left',
                timeout: 5000
            });
        }
    })
    .fail((jqXHR, textStatus) => {
        console.log(jqXHR);
        UIkit.notification({
            message: 'System Error! please contact with system manager',
            status: 'danger',
            pos: 'bottom-left',
            timeout: 5000
        });
    });
}

var pageNumber = 0;
function loadNextTickets() {
    $.ajax({
        url: '/manager/dashboard/ticket/list',
        method: 'POST',
        data: JSON.stringify({
            'num': pageNumber,
            'count': 10
        }),
        contentType: 'application/json',
        dataType: 'json'
    })
    .done((response) => {
        if (response.success) {
            pageNumber ++;

            const data = response.data;
            data.forEach(element => {
                let html = `<tr onclick="ticket_detail('${element.key}')">`;

                // auth
                if (element.auth == "auth") {
                    html += `<td class="table auth"><span class="uk-label">AUTH TICKET</span></td>`;
                } else if (element.auth == "canceling") {
                    html += `<td class="table auth"><span class="uk-label uk-label-warning">Canceling</span></td>`;
                } else if (element.auth == "cancel") {
                    html += `<td class="table auth"><span class="uk-label uk-label-danger">Cancel</span></td>`;
                } else {
                    html += `<td class="table auth"><span class="uk-label uk-label-danger">UNAUTH TICKET</span></td>`;
                }

                // username
                html += `<td class="table username">${element.username}</td>`

                // datetime
                html += `<td class="table datetime">${element.datetime}</td>`
                html += `</tr>`

                // append
                $('#table tbody').append(html);
            });
        } else {
            UIkit.notification({
                message: response.message,
                status: 'danger',
                pos: 'bottom-left',
                timeout: 5000
            });
        }
    })
    .fail((jqXHR, textStatus) => {
        UIkit.notification({
            message: 'System Error! please contact with system manager',
            status: 'danger',
            pos: 'bottom-left',
            timeout: 5000
        });
    });
}

$(document).ready(() => {
    loadNextTickets();
});
