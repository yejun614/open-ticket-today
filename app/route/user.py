from flask import Blueprint, \
                  request, \
                  session, \
                  redirect, \
                  url_for, \
                  render_template, \
                  make_response, \
                  jsonify

import json
import data

from time import gmtime, strftime, strptime, time as Time
make_datetime = lambda: strftime("%a, %d %b %Y %H:%M:%S", gmtime(Time()))

api = Blueprint('user', __name__)

@api.route('/purchase')
def purchase():
    config = {
        'contact': data.config.get('sale:contact'),
        'saletime': data.config.get('sale:time')
    }

    # Check saletime
    current_time = gmtime(Time())
    saletime = strptime(config['saletime'], "date: %Y-%m-%dT%H:%M:%S+00:00")

    if current_time >= saletime:
        return render_template('/user/purchase.html', config=config)
    else:
        return render_template('/user/purchase_ready.html', saletime=config['saletime'])

@api.route('/purchase/save', methods=['POST'])
def purchase_save():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get data
    login = {}
    username = ''
    form = {}
    seats = []
    price = ''
    datetime = make_datetime()

    try:
        login = request.json['login']
        username = request.json['username']
        form = request.json['form']
        seats = request.json['seats']
        price = request.json['price']

    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Bad request data"), 400)

    # Create ticket
    ticket = {
        'username': username,
        'seat': seats,
        'form': form,
        'price': price,
        'auth': 'unauth',
        'datetime': datetime
    }
    result = data.ticket.create(login['username'], login['password'], data=ticket, encrypt=False)

    # Return result
    if result:
        return jsonify(success=True, ticket=result)
    else:
        return jsonify(success=False, message="Wrong data header or already username is exist")


@api.route('/auth', methods=['POST'])
def user_auth():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get data
    username = ''
    password = ''
    try:
        username = request.json['username']
        password = request.json['password']

    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Bad request data"), 400)

    # Login
    result = data.ticket.login(username, password)

    if result:
        # Make session
        session['user'] = result
        session['userkey'] = 'ticket:%s:%s' % (username, password)

        return jsonify(success=True)
    else:
        return jsonify(success=False, message="Wrong ID or Password")

@api.route('/login')
def login_page():
    if 'user' in session:
        return redirect(url_for('user.ticket_page'))

    return render_template('/user/login.html')

@api.route('/logout')
def user_logout():
    session.pop('user', None)
    session.pop('userkey', None)
    return redirect(url_for('index_page'))

@api.route('/ticket')
def ticket_page():
    if 'user' not in session:
        return redirect(url_for('user.login_page'))

    user = session['user']
    docs = json.loads( data.config.get('terms') )
    cancel = data.config.get('cancel:message')

    return render_template('/user/ticket.html', \
        user=user, docs=docs, cancel=cancel)

@api.route('/ticket/gencode', methods=['GET', 'POST'])
def ticket_gen_code():
    # Check user session
    if 'user' not in session:
        return jsonify(success=False, message="Login required")

    # Check auth ticket
    if session['user']['auth'] != "auth":
        return jsonify(success=False, message="Ticket permission error")

    # Generate and return code
    result = data.ticket.gen_code(session['userkey'])
    if result:
        return jsonify(success=True, data=result)
    else:
        return jsonify(success=False, message="Can not found ticket")

@api.route('/ticket/cancel', methods=['POST'])
def ticket_cancle():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Check user session
    if 'user' not in session:
        return jsonify(success=False, message="Login required")

    # Get request data
    username = data.SHA512(session['user']['username'])
    password = ''
    try:
        password = request.json['password']
    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Data key error"), 400)

    # Check user password
    if not data.ticket.login(username, password):
        return jsonify(success=False, message="Wrong password")

    # Canceling ticket
    data.ticket.update(session['userkey'], {'auth': 'canceling'})

    # Return result
    return jsonify(success=True)
