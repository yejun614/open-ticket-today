from flask import Blueprint, \
                  request, \
                  session, \
                  make_response, \
                  jsonify

from functools import wraps

import os
import json
import data
import hashlib

api = Blueprint('seat', __name__)

def json_mime_type(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.is_json:
            return make_response(jsonify(success=False, message="Bad MIME Type"), 400)
        return f(*args, **kwargs)

    return decorated_function

@api.route('/data', methods=['GET', 'POST'])
def seat_data():
    seat = data.config.get('seat')
    width = data.config.get('seat:width')
    height = data.config.get('seat:height')
    background = data.config.get('seat:background')
    option = data.config.get('option:price')

    # to JSON
    seat = json.loads(seat)
    background = json.loads(background)
    option = json.loads(option)
    
    return jsonify(success=True, \
                   data=seat, width=width, height=height, \
                   background=background, option=option)

@api.route('/get', methods=['GET', 'POST'])
def seat_get():
    seat = data.seat.all()

    return jsonify(success=True, data=seat)

@api.route('/put', methods=['POST'])
@json_mime_type
def seat_put():
    # Password generate
    password = hashlib.sha256( os.urandom(15) ).hexdigest()

    # Get request data
    num = 0
    try:
        num = request.json['num']
    except (KeyError, ValueError, NameError):
        return make_response(jsonify(success=False, message="Bad data key"), 400)

    # Request seat
    result = data.seat.put(num, password)

    # Return result
    if result:
        return jsonify(success=True, password=password)
    else:
        return jsonify(success=False, message="Already reserved seat")

@api.route('/back', methods=['POST'])
@json_mime_type
def seat_back():
    # Get request data
    num = 0
    password = ''

    try:
        num = request.json['num']
        password = request.json['password']
    except (KeyError, ValueError, NameError):
        return make_response(jsonify(success=False, message="Bad data key"), 400)

    # Request seat cancel
    result = data.seat.pop(num, password)

    # Return result
    if result:
        return jsonify(success=True)
    else:
        return jsonify(success=False, message="Wrong password")

@api.route('/confirm', methods=['POST'])
@json_mime_type
def seat_confirm():
    # Get request data
    seats = {}
    ticket = ''

    try:
        seats = request.json['seats']
        ticket = request.json['ticket']
    except (KeyError, ValueError, NameError):
        return make_response(jsonify(success=False, message="Bad data key"), 400)

    # Confirm seats
    result = True
    for seat in seats:
        num = seat
        password = seats[seat]['password']

        result = data.seat.confirm(num, password, ticket)
        if not result: result = False

    # Result result
    return jsonify(success=result)

@api.route('/check', methods=['POST'])
@json_mime_type
def seat_check():
    # Get request data
    num = ''
    password = ''

    try:
        num = request.json['num']
        password = request.json['password']

    except (KeyError, ValueError, NameError):
        return make_response(jsonify(success=False, message="Bad data key"), 400)

    # Check seat
    result = data.seat.check(num, password)

    # Return result
    if result:
        return jsonify(success=True)
    else:
        return jsonify(success=False, message="Wrong seat password")

