import json
from functools import wraps
from flask import Blueprint, \
                  session, \
                  request, \
                  redirect, \
                  url_for, \
                  render_template, \
                  make_response, \
                  jsonify

import data

api = Blueprint('manager', __name__)

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'manager' not in session:
            return redirect(url_for('manager.manager_page'))

        return f(*args, **kwargs)

    return decorated_function

@api.route('/')
def manager_page():
    if 'manager' in session:
        return redirect(url_for('manager.manager_dashboard'))

    return render_template('/manager/login.html')

@api.route('/login', methods=['POST'])
def manager_login():
    if not request.is_json:
        return make_response(jsonify(success=False, message='Bad MIME Type'), 400)

    password = None
    try:
        password = request.json['password']

    except (KeyError, ValueError, NameError):
        return make_response(jsonify(success=False, message='Invalid data'), 400)

    # Login
    manager_pw = data.config.get('manager:password')
    if password == manager_pw:
        # Make Session (Value is logined time)
        session['manager'] = str(data.Time())

        # Return success
        return make_response(jsonify(success=True), 200)
    else:
        # Denial
        return make_response(jsonify(success=False, message='Wrong Password'), 200)

@api.route('/logout', methods=['GET', 'POST'])
def manager_logout():
    session.pop('manager', None)
    return redirect(url_for('index_page'))

@api.route('/dashboard')
@login_required
def manager_dashboard():
    return render_template('/manager/dashboard.html')

@api.route('/dashboard/ticket/list', methods=['POST'])
@login_required
def manager_ticket_list():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get request key
    num = ''
    count = ''
    try:
        num = request.json['num']
        count = request.json['count']

    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Invalid data"), 400)

    # Set start and end points
    start = num * count
    end = start + count

    # Get data
    result = data.ticket.all(start, end)

    if result:
        return jsonify(success=True, data=result)
    else:
        return jsonify(success=False, message="No more tickets")

@api.route('/dashboard/ticket/get', methods=['POST'])
@login_required
def manager_ticket_get():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get request key
    key = ''
    try:
        key = request.json['key']

    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Invalid data"), 400)

    # Get data
    result = data.ticket.get(key)

    if result:
        return jsonify(success=True, data=result)
    else:
        return jsonify(success=False, message="Can not found ticket key")

@api.route('/dashboard/ticket/password', methods=['POST'])
@login_required
def manager_ticket_password():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get request key
    key = ''
    password = ''

    try:
        key = request.json['key']
        password = request.json['password']

    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Invalid data"), 400)

    # Change password
    result = data.ticket.change_password(key, password)

    # Return result
    if result:
        return jsonify(success=True, key=result)
    else:
        return jsonify(success=False, message="Can not found ticket")

@api.route('/dashboard/ticket/delete', methods=['POST'])
@login_required
def manager_ticket_delete():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get request key
    key = ''
    try:
        key = request.json['key']
    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Invalid data"), 400)

    # Get data
    ticket = data.ticket.get(key)

    # Delete ticket
    result = data.ticket.delete(key)
    
    # Pop seats
    if ticket['auth'] != 'cancel':
        for seat in ticket['seat']:
            data.seat.pop(seat['num'], key)

    # Return result
    if result:
        return jsonify(success=True)
    else:
        return jsonify(success=False, message="Can not found ticket")

@api.route('/dashboard/ticket/update', methods=['POST'])
@login_required
def manager_ticket_update():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get request key
    key = ''
    newdata = {}
    try:
        key = request.json['key']
        newdata = request.json['data']
    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Invalid data"), 400)

    # Ticket username update
    ticket = data.ticket.get(key)
    if (ticket['username'] != newdata['username']):
        username = data.SHA512( newdata['username'] )

        key = data.ticket.change_username(key, username)

    # Update ticket detail
    result = data.ticket.update(key, newdata)

    # Check ticket auth cancel
    if newdata['auth'] == 'cancel':
        # Get data
        ticket = data.ticket.get(key)

        for seat in ticket['seat']:
            data.seat.pop(seat['num'], key)

    # Return result
    if result:
        return jsonify(success=True, key=key)
    else:
        return jsonify(success=False, message="Can not update ticket details")

@api.route('/dashboard/ticket/find', methods=['POST'])
@login_required
def manager_ticket_find():
    if not request.is_json:
        return make_response(jsonify(success=False, message="Bad MIME Type"), 400)

    # Get search query
    query = ''
    try:
        query = request.json['query']
    except (ValueError, KeyError, NameError):
        return make_response(jsonify(success=False, message="Invalid data"), 400)

    # Check privous result
    search = []

    if 'search_query' in session:
        try:
            if query.index(session['search_query']) == 0:
                search = session['search']
        except ValueError:
            # Search data
            keys = [i for i in data.database.scan_iter('ticket:*')]
            search = []
            for key in keys:
                d = json.loads( data.database.get(key) )
                d['key'] = key.decode()

                search.append( json.dumps(d) )

    # Searching
    new_index = []
    result = []
    for ticket in search:
        if query in ticket:
            new_index.append( ticket )
            result.append( json.loads(ticket) )

    # Set session
    session['search_query'] = query
    session['search'] = new_index

    # Return result
    return jsonify(success=True, data=result)

@api.route('/check')
@login_required
def manager_check_page():
    return render_template('/manager/check.html')

@api.route('/check/code', methods=['POST'])
@login_required
def manager_check_code():
    if not request.is_json:
        return make_response(jsonify(success=False, message='Bad MIME Type'), 400)

    # Get code
    code = ''
    try:
        code = request.json['code']

    except (KeyError, ValueError, NameError):
        return make_response(jsonify(success=False, message='Invalid data'), 400)

    # Check code
    result = data.ticket.check_code(code)

    # Return result
    if result:
        return make_response(jsonify(success=True, data=result), 200)
    else:
        return make_response(jsonify(success=False, message="Can not found the code"), 200)
