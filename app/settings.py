config = {
    "manager:password": "2b64f2e3f9fee1942af9ff60d40aa5a719db33b8ba8dd4864bb4f11e25ca2bee00907de32a59429602336cac832c8f2eeff5177cc14c864dd116c8bf6ca5d9a9",
    "terms": [
        {"id": "docs_id", "title": "Title", "content": "HTML Content"},
        {"id": "docs_id2", "title": "Title2", "content": "Content example"}
    ],
    "form": [
        {"type": "text", "name": "Bank Account Holder", "value": ""},
        {"type": "radio", "name": "Select", "option": ["option1", "option2", "option3"], "checked": "0"},
        {"type": "checkbox", "name": "Accept ?", "value": "accept"}
    ],
    "cancel:message": "<span>Cancel message</span>",
    "seat:length": 5,
    "seat": [
        {"num": 0, "position": "(10, 20)", "option": ["Normal", "Hard"]},
        {"num": 1, "position": "(30, 20)", "option": ["Normal", "Hard"]},
        {"num": 2, "position": "(50, 20)", "option": ["Normal", "Hard"]},
        {"num": 3, "position": "(70, 20)", "option": ["Normal", "Hard"]},
        {"num": 4, "position": "(10, 40)", "option": ["Normal"]},
        {"num": 5, "position": "(30, 40)", "option": ["Normal"]}
    ],
    "seat:width": "1000",
    "seat:height": "400",
    "option:price": {
        "Normal": "KRW 1000",
        "Hard": "KRW 5000"
    },
    "seat:background": [],
    "sale:time": "date: 2020-03-06T12:50:00+00:00",
    "sale:contact": "Email: test@test / Tel: 000-0000-0000 / Company: Test / Address: addr"
}
